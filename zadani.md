# Projekt Mood Lamp
* Zadání: v týdnu **3.10.-7.10.2022**
* Termín produkt: příslušné cvičení v týdnu **28.11.-2.12.2022**
* Termín prezentace: příslušné cvičení v týdnu **5.12.-9.12.2022**

## Výstup
* produkt splňující minimální požadavky
* prezentace funkčnosti před třídou
* repozitář na GIT s dokumentací
* video prezentující projekt o délce *min. 1. minuty*

## Minimální požadavky
* Vhodný vlastní design
  * 3D tisk - není povinný
* Základ postavený na ESP
* Použití RGB
  * min. 2x LED / pásek (Neopixel) / kruh
* Měření teploty a vlhkosti
  * doporučený modul DHT11 - digitální výstup
* Měření osvětlení
* Webový frontend
  * Ovládání RGB kanálů
  * Přepínání efektů / přednastavených barev
  * Výpis stavu
    * RGB
    * Teplota
    * Vlhkost
    * Osvětlení
		
## Možná rozšíření
* I2C LCD
* Napájení z baterie
* Tlačítka
* RTC - > nastavení jako budík
* OTA

## Postup průběžně dokumentujte pomocí
* Nákresů
* Fotky
* Videa

## Inspirace:
[Instructables - návod na RGB Lampu](https://www.instructables.com/circuits/howto/rgb+lamp/)

