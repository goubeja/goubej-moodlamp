# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
* jak dlouho mi tvorba zabrala - **čistý čas** 15-20 hodin
* jak se mi to podařilo rozplánovat - velmi zajímavě
* z čeho se to skládá - elektronika 
    Neopixel LED pásek, teplotní a vlhkostní senzor DHT11, Fotorezistor, rezistory z aliexpresu, Wemos R1D2, kabely
* jaký materiál na výrobu byl použit
    psací stroj Consul, lepící páska, hodně nervů
* proč to tak vypadá - kreativita

* co se mi povedlo
    Originální nápad
* co se mi nepovedlo/příště bych udělal/a jinak
    Naučil bych se pájet a programovat
* co mě na tom (ne)bavilo